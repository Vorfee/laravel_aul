<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

/**
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
    return view('licornes.index', [
        'licornes' => Licorne::all()
    ]);
}

/**
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{
    return view('licornes.create');
}

/**
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{
    $Licorne = $request->user()->create([
        'title' => $request->input('title'),
        'content' => $request->input('content')
    ]);
    return redirect()->route('licornes.show', [$Licorne]);
}

/**
     *
     * @param  \App\Licorne  $Licorne
     * @return \Illuminate\Http\Response
     */
public function show(Licorne $Licorne)
{
    return view('licornes.show', [
        'Licorne' => $Licorne
    ]);
}

/**
 *
 * @param  \App\Licorne  $Licorne
 * @return \Illuminate\Http\Response
 */
public function edit(Licorne $Licorne)
{
    return view('licornes.update', [
        'Licorne' => $Licorne
    ]);
}

/**
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \App\Licorne  $Licorne
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, Licorne $Licorne)
{
    $Licorne->update([
        'title' => $request->input('title'),
        'content' => $request->input('content')
    ]);
    return redirect()->route('licornes.show', [$Licorne]);
}

/**
 *
 * @param  \App\Licorne  $Licorne
 * @return \Illuminate\Http\Response
 */
public function destroy(Licorne $Licorne)
{
    $Licorne->delete();
    return redirect()->route('licornes.index');
}

}
